<?
function GetGVariable($str) {
	if ( isset($_GET) ) {
		if ( $str != '' ) {
			return isset($_GET["$str"]) ? $_GET["$str"] : '';
		} else {
			return $_GET;
		}
	} else {
		global $HTTP_GET_VARS;

		if ( $str != '' ) {
			return isset($HTTP_GET_VARS["$str"]) ? $HTTP_GET_VARS["$str"] : '';
		} else {
			return $HTTP_GET_VARS;
		}
	}
}
function SetGVariable($str, $value) {
	if ( isset($_GET) ) {
		$_GET["$str"] = "$value";
	} else {
		global $HTTP_GET_VARS;
		$HTTP_GET_VARS["$str"] = "$value";
	}
}

function GetPostVariable($str) {
	if ( isset($_POST) ) {
		if ( $str != '' ) {
			return isset($_POST["$str"]) ? $_POST["$str"] : '';
		} else {
			return $_POST;
		}
	} else {
		global $HTTP_POST_VARS;

		if ( $str != '' ) {
			return isset($HTTP_POST_VARS["$str"]) ? $HTTP_POST_VARS["$str"] : '';
		} else {
			return $HTTP_POST_VARS;
		}
	}
}
function SetPostVariable($str, $value) {
	if ( isset($_POST) ) {
		$_POST["$str"] = "$value";
	} else {
		global $HTTP_POST_VARS;
		$HTTP_POST_VARS["$str"] = "$value";
	}
}

function GetServerVariable($str) {
	if ( isset($_SERVER) ) {
		if ( $str != '' ) {
			return isset($_SERVER["$str"]) ? $_SERVER["$str"] : '';
		} else {
			return $_SERVER;
		}
	} else {
		global $HTTP_SERVER_VARS;
		if ( $str != '' ) {
			return isset($HTTP_SERVER_VARS["$str"]) ? $HTTP_SERVER_VARS["$str"] : '';
		} else {
			return $HTTP_SERVER_VARS;
		}
	}
}
function SetServerVariable($str, $value) {
	if ( isset($_SERVER) ) {
		$_SERVER["$str"] = "$value";
	} else {
		global $HTTP_SERVER_VARS;
		$HTTP_SERVER_VARS["$str"] = "$value";
	}
}

function GetEnvVariable($str) {
	if ( isset($_ENV) ) {
		if ( $str != '' ) {
			return isset($_ENV["$str"]) ? $_ENV["$str"] : '';
		} else {
			return $_ENV;
		}
	} else {
		global $HTTP_ENV_VARS;
		if ( $str != '' ) {
			return isset($HTTP_ENV_VARS["$str"]) ? $HTTP_ENV_VARS["$str"] : '';
		} else {
			return $HTTP_ENV_VARS;
		}
	}
}

function GetSessionVariable($str) {
	if ( isset($_SESSION) ) {
		if ( $str != '' ) {
			return isset($_SESSION["$str"]) ? $_SESSION["$str"] : '';
		} else {
			return $_SESSION;
		}
	} else {
		global $HTTP_SESSION_VARS;
		if ( $str != '' ) {
			return isset($HTTP_SESSION_VARS["$str"]) ? $HTTP_SESSION_VARS["$str"] : '';
		} else {
			return $HTTP_SESSION_VARS;
		}
	}
}
function SetSessionVariable($str, $value) {
	if ( isset($_SESSION) ) {
		$_SESSION["$str"] = "$value";
	} else {
		global $HTTP_SESSION_VARS;
		$HTTP_SESSION_VARS["$str"] = "$value";
	}
}
function UnsetSession($str) {
	if ( isset($_SESSION) ) {
		if ( $str != '' && isset($_SESSION["$str"]) ) { unset($_SESSION["$str"]); }
	} else {
		global $HTTP_SESSION_VARS;
		if ( $str != '' && isset($HTTP_SESSION_VARS["$str"]) ) { unset($HTTP_SESSION_VARS["$str"]); }
	}
}

function GetRequestVariable($str) {
	if ( isset($_REQUEST) ) {
		if ( $str != '' ) {
			return isset($_REQUEST["$str"]) ? $_REQUEST["$str"] : '';
		} else {
			return $_REQUEST;
		}
	} else {
		global $HTTP_GET_VARS,$HTTP_POST_VARS,$HTTP_COOKIE_VARS;
		if ( $str != '' ) {
			if ( isset($HTTP_GET_VARS["$str"]) ) {
				return $HTTP_GET_VARS["$str"];
			} elseif ( isset($HTTP_POST_VARS["$str"]) ) {
				return $HTTP_POST_VARS["$str"];
			} elseif ( isset($HTTP_COOKIE_VARS["$str"]) ) {
				return $HTTP_COOKIE_VARS["$str"];
			} else {
				return '';
			}
		} else {
			return array_merge(GetGVariable(''), GetPostVariable(''), $HTTP_COOKIE_VARS);
		}
	}
}

function GetFileVariable($str) {
	if ( isset($_FILES) ) {
		if ( $str != '' ) {
			return isset($_FILES["$str"]) ? $_FILES["$str"] : '';
		} else {
			return $_FILES;
		}
	} else {
		global $HTTP_POST_FILES;
		if ( $str != '' ) {
			return isset($HTTP_POST_FILES["$str"]) ? $HTTP_POST_FILES["$str"] : '';
		} else {
			return $HTTP_POST_FILES;
		}
	}
}


    class NOF_resource {
        var $database;
        var $dictFiles;
        var $errors;
        var $path;

        function NOF_resource() {
            $this->database = array();
            $this->errors = array();
            $this->dictFiles = array();
        }

        function addFile($dictFile) {
            array_push($this->dictFiles,array("path"=>dirname($dictFile),"filename"=>basename($dictFile)));
            $this->parse($dictFile);
            if (count($this->errors)>0) {
                return false;
            } else {
                return true;
            }
        }

        function parse($dictFile) {
            if (!$file = @file($dictFile)) {
                array_push($this->errors,"Failed to open '$dictFile'");
            }
            foreach ($file as $key=>$value) {
                $key = addslashes($key);
                $value = addslashes($value);
                if ( trim($value) != '' && substr(ltrim($value), 0, 1) != '#' ) {
	                if (strpos(strtolower($value),'import')>0) {
	                    $g = array();
	                    preg_match('/^\s*@import\s+["\']([^"]+)["\']/i',stripslashes($value),$g);
	                    if (count($g) > 0) {
	                        $this->parse($this->dictFiles[count($this->dictFiles)-1]['path'] . '/' . $g[1]);
	                    }
	                } else {
	                    list($dictKey,$dictValue) = explode('=',$value);
	                    if ($dictKey != '' && $dictValue !='') {
	                        $this->database[strtolower($dictKey)] = rtrim($dictValue);
	                    }
	                }
                }
            }
        }

        function get($key,$params=array()) {
            $key = addslashes($key);
            if ( isset($this->database[strtolower($key)]) ) {
            	$value = $this->database[strtolower($key)];
                if (substr($value,0,1) == '$') {
                    $value = $this->get(substr($value,1));
                }

				if ( count($params) > 0 ) {
					$arrKeys = array_keys($params);
					$arrVals = array_values($params);

					for ( $i=0; $i<count($arrKeys); $i++ ) {
						$value = str_replace($arrKeys[$i], $arrVals[$i], $value);
					}
				}
//                $value = str_replace(array_keys($params),array_values($params),$value);
                $value = preg_replace("/\{[0-9]{1}\}/i","",$value);
                $value = str_replace("  "," ",$value);
                return stripslashes($value);
            } else {
            	return $key;
            }
/*
            if (!array_key_exists(strtolower($key),$this->database)) {
                return $key;
            } else {
                $value = $this->database[strtolower($key)];
                if (substr($value,0,1) == '$') {
                    $value = $this->get(substr($value,1));
                }
                $value = str_replace(array_keys($params),array_values($params),$value);
                $value = preg_replace("/\{[0-9]{1}\}/i","",$value);
                $value = str_replace("  "," ",$value);
                return stripslashes($value);
            }
*/
        }
    }


    function NOF_fileExists($incfile) {
        global $nof_suiteName;
        if (file_exists($incfile)) {
            return true;
        } else {
            NOF_throwError(103,array("{1}"=>NOF_mapPath($incfile),"{2}"=>$nof_suiteName),false);
            return false;
        }
    }

    function NOF_throwError($code,$params=array(),$stop=true) {
        global $nof_resources;
        if($GLOBALS["nof_debug"] == "true") {
            echo "<p>" . $nof_resources->get("Error.".$code,$params) . "</p>";
        } else {
            echo "<p>" . $nof_resources->get("Error.Text.Generic") . "</p><p>" . $nof_resources->get("Error.Text.Code") . " $code</p>";
        }
        if ($stop) {
            exit();
        }
    }

    // c:           /   inetpub /   wwwroot /   texte   /   formshandler
    // ..           /   db      /   ..      /   scripts /   .
    // ..(getcwd)   /   db      /
    function NOF_mapPath($path) {
        // if $path is already a path, return it as is
        if ($path[0] == '/' || $path[1] == ':') {
            return addslashes($path);
        }
        // if $path exists, it's quite easy
        if ($mapPath = realpath(($path))) {
            return addslashes($mapPath);
        } else {
            // get current directory as full path
            $dir = getcwd();
            // workaround on path separator
            if (strpos($dir,'\\')>0) {
                $pathSep = '\\';
                $path  = str_replace('/',$pathSep,$path);
            } else {
                $pathSep = '/';
                $path  = str_replace('\\',$pathSep,$path);
            }
            // build a full  path
            $mapPath = $dir . $pathSep . $path;
            // split it
            $mapDirs = explode($pathSep,$mapPath);
            // treat . and ..
            for ($i=0;$i<count($mapDirs);$i++) {
                if ($mapDirs[$i+1] == '..') {
                    unset($mapDirs[$i]);
                    unset($mapDirs[$i+1]);
                }

                if (isset($mapDirs[$i])) {
                    if ($mapDirs[$i] == '.') {
                        unset($mapDirs[$i]);
                    }
                }
            }
            return addslashes(join($pathSep,$mapDirs));
        }
    }

    if (!isset($nof_resources)) {
        $nof_resources = new NOF_resource();
    }

	if ( !function_exists('html_entity_decode') ) {
		function html_entity_decode($str) {
			$htmlEntities = array('<p>', '</p>', "<br>");
			$htmlEntitiesValue = array('\r\n', '\r\n', '\r\n');

			for ( $i=0; $i<count($htmlEntities); $i++ ) {
				$str = str_replace($htmlEntities[$i], $htmlEntitiesValue[$i], $str);
			}
			return $str;
		}
	}

    function encodeData($str) {
		$str = str_replace('\\\\', "&slsh;" , $str);
		$str = str_replace('\\"', '&quot;' , $str);
		$str = preg_replace('/([^{\\}])"/', '$1\"', $str);
		$str = preg_replace('/\r\n/', '\n', $str);
		$str = preg_replace('/\n/', '\n', $str);
		$str = utf8_encode($str);

		return $str;
	}

    function decodeData($str) {
		$str = str_replace('\\\\', "\\" , $str);
		$str = str_replace('&slsh;', '\\\\' , $str);
		$str = str_replace('&quot;', '\"' , $str);
		$str = utf8_decode($str);

		return $str;
	}

	function encodeURI($str) {
		$str = str_replace(" ", "%20", $str);
		return $str;
	}
?>
