<?php
error_reporting(0);

if ( !file_exists('nof_utils.inc.php') ) {
	echo "<p>An error occured. Please contact the site administrator</p>
		  <p>Error code: 103</p>";
}
require_once("nof_utils.inc.php");

$nof_rootDir = GetPostVariable('nof_rootDir');
$nof_debug = GetPostVariable('nof_debug');
$nof_componentId = GetPostVariable('nof_componentId');
$nof_scriptDir = GetPostVariable('nof_scriptDir');
$nof_scriptInterfaceFile = GetPostVariable('nof_scriptInterfaceFile');
$cgiDir = "";
$nof_suiteName = "FormsHandler";
$nof_langFile = GetPostVariable('nof_langFile');

if ( !isset($nof_debug) ) $nof_debug = 'true';
if ( !file_exists($nof_langFile) ) {
    if($nof_debug == 'true') {
        echo "<p>DEBUGINFO: Some components were left unpublished. Please check your publish settings in Fusion and republish the site.</p>";
    } else {
        echo "<p>An error occured. Please contact the site administrator</p>
              <p>Error code: 103</p>";
    }
}

$nof_resources->addFile($nof_langFile);


if (NOF_fileExists('fh_xmlparser.php')) {
      require_once('fh_xmlparser.php');
} else {
      exit();
}
//global variables

if( GetPostVariable('nof_scriptInterfaceFile')=='' ) {
    $errorMessage = "Expected POST param not passed: nof_scriptInterfaceFile";
    NOF_throwError(800,array("{1}"=>"nof_scriptInterfaceFile"));
    exit();
}
$xmlPropertyFile   = GetPostVariable('nof_scriptInterfaceFile');

if(!file_exists($xmlPropertyFile)) {
    NOF_throwError(620,array("{1}"=>NOF_mapPath($xmlPropertyFile),"{2}"=>NOF_mappath(getcwd())));
    exit();
}


$conf                = "";
$errorField          = "";
$errorLabel          = "";
$fieldsDBArray       = "";
$firstLineUD         = "";
$FHCompIdent         = "formshandler" . "."  . GetPostVariable('nof_componentId') . ".";


//include file with common functions.
//put this include always after the
//global variables cause the functions
//need the global variables
if (NOF_fileExists('fh_common.php')) {
      require_once('fh_common.php');
} else {
      exit();
}



//reset the error flag
$errorFlag=FALSE;

//read the site property file into the $conf array
//read contents of forms handler script properties file into the $conf array
//read the configuration file for the forms handler script into the $conf array
//read the email template into the $conf array
//read the XML property file
$conf = fhParseXmlFile($xmlPropertyFile);

$emailPropertiesFile = 'fh_emailTemplate_'.strtolower($conf[$FHCompIdent.'language']).'.properties';
readEmailTemplate($emailPropertiesFile, "[EMAIL]", $FHCompIdent);

if( GetPostVariable('nof_componentId')=='' ) {
      NOF_throwError(800,array("{1}"=>'nof_componentId'));
      exit();
}


//get column names in the db into a global array
getExpectedDBFields($FHCompIdent);


//if the required parameters were unfilled
if ( fhCheckIfRequiredUnfilled() ) {
      //display error page
      fhDisplayErrorPage($errorLabel,$errorField);
      exit();
}

//if fields are not within their length limitations
if( fhCheckIfFieldsLengthInvalid() ) {
      //set error flag to true
      $errorFlag = TRUE;
}

//if uploaded files had any errors
if( checkIfUploadedFilesInvalid($FHCompIdent) ) {
      //set error flag to true
      $errorFlag = TRUE;
}

//if there was error
if( $errorFlag ) {
      //display the error page
      fhDisplayErrorPage($errorLabel, $errorField);
      exit();
} else {
      //if no errors were encountered in user input
      //store the uploaded file in the directory specified in the property file
      getUploadedFile($FHCompIdent);
      if($conf[$FHCompIdent."writeToDB"] == "true" && $conf[$FHCompIdent."dbPath"] != '') {
            if (!file_exists(dirname($conf[$FHCompIdent . "dbPath"]))) {
                NOF_throwError(540,array("{1}"=>NOF_mapPath(dirname($conf[$FHCompIdent . "dbPath"])),"{2}"=>NOF_mappath(getcwd())));
            }

            if (!is_writable(dirname($conf[$FHCompIdent . "dbPath"]))) {
                NOF_throwError(541,array("{1}"=>NOF_mapPath(dirname($conf[$FHCompIdent . "dbPath"])),"{2}"=>NOF_mappath(getcwd())));
            }
            //check if DB needs to be created/overwritten
            //check if DB needs to be created/overwritten
            if (!checkIfDBmatch($FHCompIdent)) {
                  //open DB file for writing

                  if (!$FILE = @fopen($conf[$FHCompIdent."dbPath"], 'wb')) {
                        NOF_throwError(500,array("{1}"=>NOF_mapPath($conf[$FHCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$FHCompIdent . "dbPath"]))));
                        exit();
                  } else {
                        //write first line of UD file, to the DB file
                        if (!fputs($FILE, "$firstLineUD\n")) {
                              NOF_throwError(502,array("{1}"=>NOF_mapPath($conf[$FHCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$FHCompIdent . "dbPath"]))));
                              exit();
                        }
                        //close file handle
                        fclose($FILE);
                  }
            }
      }
      //insert a row into the database with the form value
      $entry = fhInsertInDB();
      //send an email with the form values
      if($conf[$FHCompIdent."sendEmail"] == "true") {
            fhSendFormEmail($entry);
      }
      //display the success page
      fhDisplaySuccessPage();
}



//display the success page after a successful submission
function fhDisplaySuccessPage() {
      global $conf, $FHCompIdent, $errors;
      if($errors) {
            return;
      }

      //make the path of the next page gathered from the property file relative to the script
      $conf[$FHCompIdent."nextPage"] = makeRelativeToCgiBin($conf[$FHCompIdent."nextPage"]);

      //print out a javascript form which directs the top frame to the next page
      echo "<html>";
      echo "<head></head>";
      echo "<body></body>";
      echo "<script>";
      echo "top.document.location.href='" .  $conf[$FHCompIdent."nextPage"] . "';" ;
      echo "</script>";
      echo "</html>";
}



//insert the field values in the DB
function fhInsertInDB() {
      global $conf,$requiredForPassRetv,$fieldsDBArray,$firstLineUD,$FHCompIdent,$errors;

      //for every expected field in the DB get its value from post request
      for($i=0; $i<count($fieldsDBArray); $i++) {
            //check if the input is of type file
            if ( GetFileVariable($fieldsDBArray[$i])!='' ) {
				$postFile = GetFileVariable($fieldsDBArray[$i]);
                  //if the input was left blank
                  if(empty($postFile['name'])) {
                        $entryAppend = "";
                  } else {
                        //get the path of the uploaded file
                        //just put the name of the file in the database
                        $entryAppend = $postFile['name'];
                  }
                  if(!isset($entry)) {
                        $entry = '"' . $entryAppend . '"';
                  } else {
                        $entry = $entry . "," .  '"' . $entryAppend . '"';
                  }
            }
            //if input is not of type file
            else {
                  //check if the param has multiple values (from a checkbox group for example)
                  if(gettype(GetPostVariable($fieldsDBArray[$i])) == "array") {
                        $entryAppend = "";
                        $postVar = GetPostVariable($fieldsDBArray[$i]);
                        for($j=0; $j<count($postVar); $j++) {
                              if($entryAppend == "") {
                                    $entryAppend = $postVar[$j];
                              } else {
                                    //multiple values are clubbed together seperated by commas
                                    $entryAppend = $entryAppend . "," . $postVar[$j];
                              }
                        }
                  } else {  //if the param is not multiple valued
                        $entryAppend = GetPostVariable($fieldsDBArray[$i]);
                  }

                  if(!isset($entry)) {
                        $entry = preprocess($entryAppend);
                  } else {
                        $entry = $entry . "," .  preprocess($entryAppend);
                  }

            }

      }

      //if creator of site chose to write into database
      if($conf[$FHCompIdent."writeToDB"] == "true" && !$errors) {
            //open DB file for appending
            if (!$FILE = @fopen($conf[$FHCompIdent."dbPath"], 'ab')) {
                  NOF_throwError(502,array("{1}"=>NOF_mapPath($conf[$FHCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$FHCompIdent . "dbPath"]))));
                  exit();
            } else {
                  //append entry to the file
                  if (!fputs($FILE, "$entry\n")) {
                        NOF_throwError(502,array("{1}"=>NOF_mapPath($conf[$FHCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$FHCompIdent . "dbPath"]))));
                        exit();
                  }
                  //close file handle
                  fclose($FILE);
            }
      }

      return ($entry);
}



//function that takes the upload dir and
//filename and returns the upload path
//also it changes the name of the file in
//the global file array
function fhSetUploadFilePath($dbFileUploadDir, $fileName, $key) {
      global $conf;

      $uploadFilePath = $dbFileUploadDir . "/" . $fileName;

      $i = 1;
      while(file_exists($uploadFilePath)) {
            $fileNameArr =  explode("." , $fileName);
            if(isset($fileNameArr[1])) {
                  $newFileName = $fileNameArr[0] . $i . "." . $fileNameArr[1];
            } else {
                  $newFileName = $fileNameArr[0] . $i;
            }
            $uploadFilePath = $dbFileUploadDir . "/" . $newFileName;
            $postFile = GetFileVariable($key);
            $postFile['name'] = $newFileName;
            $i++;
      }

      return $uploadFilePath;
}



/*
* function to call the error script to display the error page
*/
function fhDisplayErrorPage($error_label, $error_field) {
      global $conf,$FHCompIdent;

      //reset the post array
      $postArr = GetPostVariable('');
      reset($postArr);
      //go through the posted entities and make a list (comma seperated) of http names and
      //a list of http values (,;,; seperated)
      while(list($key,)=each($postArr)) {
            //if the param has multiple values (for example a checkbox group)
            if(gettype($postArr[$key]) == "array") {
                  for($i=0; $i<count($postArr[$key]); $i++) {
                        if(!isset($params)) {
                              $params = $key;
                        } else {
                              $params = $params . "," . $key;
                        }
                        if(!isset($values)) {
                              $values = $postArr[$key][$i];
                        } else {
                              $values = $values . ",;,;" . $postArr[$key][$i];
                        }
                  }
            } else { //if param was not multiple valued
                  if(!isset($params)) {
                        $params = $key;
                  } else {
                        $params = $params . "," . $key;
                  }
                  if(!isset($values)) {
                        $values = $postArr[$key];
                  } else {
                        $values = $values . ",;,;" . $postArr[$key];
                  }
            }
      }

      //get the error labels and associated fields in arrays
      $errorLabel = explode(",",$error_label);
      $errorField = explode(",",$error_field);

      //make a page that hides all the necessary values for error handling and
      //posts it to the forms page
      echo "<HTML>";
      echo "<HEAD></HEAD>";
      echo "<BODY>";

      echo "<FORM NAME=\"INV\" METHOD=\"POST\" ACTION=\"" . GetServerVariable('HTTP_REFERER') . "\" target=\"_self\">" ;
      //for each error
      $errorMessages = '';
      for($i=0; $i< count($errorLabel); $i++) {
            $errorLabelMinusMessage = preg_replace("/\.message$/i","",$errorLabel[$i]);
            $errorMessages =  $errorMessages . "<li><SPAN CLASS=\"" . $conf[$errorLabelMinusMessage . ".errorcss"] . "\">"
            . cleanField($conf[$errorLabel[$i]])
            . "</SPAN>"
            . "</li>";
      }

      //list of error messages
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"" . GetPostVariable('nof_componentId') . "_"  . "errormessgs" . "\" VALUE='<ul>" . $errorMessages . "</ul>'>" ;
      //list of error images
      for($i=0; $i< count($errorField); $i++) {
            if (!empty($conf[$FHCompIdent."errorMark"])) {
                  echo "<INPUT TYPE=\"HIDDEN\" NAME=\"" . GetPostVariable('nof_componentId') . "_"  . $errorField[$i] . "_errorimg" . "\" VALUE=\"" . "<img src='" . $conf[$FHCompIdent."errorMark"] . "' alt='' >"  . "\">";
            } else {
                  echo "<INPUT TYPE=\"HIDDEN\" NAME=\"" . GetPostVariable('nof_componentId') . "_" . $errorField[$i] . "_errorimg" . "\" VALUE=\"" . "\">";
            }
      }

      //list of params on page
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"PARAMS\" VALUE=\"" . encodeData($params) . "\">" ;
      //list of param values on page
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"VALUES\" VALUE=\"" . encodeData($values) . "\">" ;
      //form name of the form
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"FORMNAME\" VALUE=\"formshandler" . GetPostVariable('nof_componentId') . "\">" ;

      echo "</FORM>";
      echo "</BODY>";
      echo "<script type=\"text/javascript\">";
      echo "document.forms[0].submit();";
      echo "</SCRIPT>";
      echo "</HTML>";
}



/*
* function to see if all required parameters were filled
*/
function fhCheckIfRequiredUnfilled() {
      global $conf, $fieldsDBArray, $FHCompIdent;

      //reset error flag to false
      $errorFoundFlag = FALSE;

      //for every expected field in the DB
      for($i=0; $i<count($fieldsDBArray); $i++) {
            $property = $FHCompIdent . $fieldsDBArray[$i] . ".errorevent.required.active";

            //if the db field is specified as required in the form handler property file
            if($conf[$property] == "true") {
                  $postVar = '';
                  if ( GetPostVariable($fieldsDBArray[$i])!='' ) { $postVar = GetPostVariable($fieldsDBArray[$i]); }
                  $postFile = GetFileVariable($fieldsDBArray[$i]);
                  //check if non multiple http params are non empty
                  if( $postVar!='' && gettype($postVar)!="array" && (! preg_match("/^\s*$/",$postVar)) ) {
                        continue;
                  }
                  //check if http params with multiple values are non empty
                  elseif( is_array($postVar) && isset($postVar[0]) ) {
                        continue;
                  }
                  //check if a file with that http name is sent
                  elseif( isset($postFile['name']) && !empty($postFile['name']) ) {
                        continue;
                  } else { //if empty
                        //the label associated with the error messages
                        //$label="label." . $fieldsDBArray[$i] . ".error" . ".required" ;
                        $label= $FHCompIdent . $fieldsDBArray[$i] . ".errorevent.required.message" ;
                        //club the error label and error field
                        clubError($label,$fieldsDBArray[$i]);
                        //set the error found flag to true
                        $errorFoundFlag=TRUE;
                  }

            }
      }

      return $errorFoundFlag;
}



//check if the field lengths are within their limits
function fhCheckIfFieldsLengthInvalid() {
      global $conf, $FHCompIdent;
      //set the error found fla to false
      $errorFoundFlag = FALSE;
      //reset the post array
      $postArr = GetPostVariable('');
      reset($postArr);
      //go throuh the posted entities
      while( list($field,$value) = each($postArr) ) {

            $shortErrorEventActiveProperty = $FHCompIdent . $field . ".errorevent.short.active";
            $minimumLengthProperty = $FHCompIdent . $field . ".errorevent.short.minimumlength";

            //if the field is specified as required in the property file
            if (!isset($conf[$shortErrorEventActiveProperty])) {
                  $conf[$shortErrorEventActiveProperty] = "false";
            }
            if(!empty($postArr[$field])
            && $conf[$shortErrorEventActiveProperty] == "true"
            && preg_match("/^[-+]{0,1}\d+$/" ,$conf[$minimumLengthProperty])
            && strlen($postArr[$field]) < $conf[$minimumLengthProperty] ) {
                  //the label associated with the error messages
                  $label= $FHCompIdent . $field . ".errorevent.short.message" ;
                  //club the error label and error field
                  clubError($label,$field);
                  //set the error found flag to true
                  $errorFoundFlag=TRUE;
            }

            $longErrorEventActiveProperty = $FHCompIdent . $field . ".errorevent.long.active";
            $maximumLengthProperty = $FHCompIdent . $field . ".errorevent.long.maximumlength";

            if (!isset($conf[$longErrorEventActiveProperty])) {
                  $conf[$longErrorEventActiveProperty] = "false";
            }

            if(!empty($postArr[$field])
            && $conf[$longErrorEventActiveProperty] == "true"
            && preg_match("/^\+{0,1}\d+$/" ,$conf[$maximumLengthProperty] )
            && strlen($postArr[$field]) > $conf[$maximumLengthProperty] ) {
                  //the label associated with the error messages
                  $label = $FHCompIdent . $field . ".errorevent.long.message" ;
                  //club the error label and error field
                  clubError($label,$field);
                  //set the error found flag to true
                  $errorFoundFlag = TRUE;
            }
      }

      return $errorFoundFlag;
}



//send form data and uploaded files in email
function fhSendFormEmail($entry) {
      global $conf, $fieldsDBArray, $FHCompIdent;

      //get email template in a single line
      $body = $conf["[EMAIL]Body"];

    $entry = substr($entry,1,strlen($entry)-3);
      //get the individual column entries of the database
      $entryPieces = split("\"\,\"",$entry);

      //for each column entry
      for ( $i = 0; $i < count( $entryPieces ); $i++ ) {
            //replace place holders with the account and also let a empty place holder be at the end for subsequent values
            if ( eregi("(\{beginiterator\})(.*)(\{0\})(.*)(\{enditerator\})", $body, $match) ) {
                  $body = str_replace($match[0], $match[2] . $fieldsDBArray[$i] . ": " . $entryPieces[$i] . $match[4] . $match[0], $body);
            }
      }

      //remove the empty placholder for the accounts
      $body = eregi_replace("\{beginiterator\}(.*)\{enditerator\}", "" , $body);
      $body = str_replace("\\n", "\n", $body);
      $body = str_replace("\\", "", $body);

      if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $conf[$FHCompIdent . "emailToAddress"])) {
            NOF_throwError(202,array("{1}"=>$conf[$FHCompIdent . "emailToAddress"]));
            exit;
      }
      if (NOF_fileExists("fh_mailer.php")) {
            include_once("fh_mailer.php");
      } else {
            exit();
      }

      $mail = new PHPMailer();
      $mail->From      = $conf[$FHCompIdent . "emailFromAddress"];
      $mail->FromName  = $conf[$FHCompIdent . "emailFromAddress"];
      $mail->Host      = $conf[$FHCompIdent . "emailServer"];
      $mail->Port      = $conf[$FHCompIdent . "emailServerPort"];
      $mail->SMTPDebug = false;
      if (defined('DEBUG')) {
            $mail->SMTPDebug = true;
      }
      $mail->Mailer    = "smtp";
      $mail->IsHTML    = true;
      $mail->Subject   = $conf["[EMAIL]Subject"];
      $mail->CharSet   = "ISO-8859-1";
      $mail->SMTPAuth  = false;
      $mail->AddAddress($conf[$FHCompIdent . "emailToAddress"],$conf[$FHCompIdent . "emailToAddress"]);
      $mail->Body      = $body;


      // process attachments
      $postFiles = GetFileVariable('');
      reset($postFiles);
      while( list($key,) = each($postFiles) ) {
            if( !empty($postFiles[$key]['name']) ) {
                  $mail->AddAttachment($conf[$FHCompIdent . "dbFileUploadDir"] . "/" . $postFiles[$key]['name'], $postFiles[$key]['name'], "base64", $postFiles[$key]['type']);
            }
      }

      // send e-mail
      if (!$mail->Send()) {
        echo "<!-- ErrorInfo: ".$mail->ErrorInfo."-->";
        $mail->Mailer = "mail";
        if (!$mail->Send()) {
            echo "<!-- ErrorInfo: ".$mail->ErrorInfo."-->";
            NOF_throwError(201,array("{1}"=>$conf[$FHCompIdent . "emailToAddress"],"{2}"=>$conf[$FHCompIdent . "emailFromAddress"],"{3}"=>$conf[$FHCompIdent."emailServer"].":".$conf[$FHCompIdent."emailServerPort"],"{4}"=>$mail->ErrorInfo));
            exit();
        }
      }

      // done! clean up
      $mail->ClearAddresses();
      $mail->ClearAttachments();
      return true;
}

?>
