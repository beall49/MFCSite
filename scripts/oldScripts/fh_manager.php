<?php

$cgiDir = $nof_rootDir ."/". $nof_scriptDir;
$componentid = $nof_componentId;
$xmlPropertyFile   = $cgiDir . "/" . $nof_scriptInterfaceFile;


if (NOF_fileExists($cgiDir . '/fh_xmlparser.php')) {
      require_once($cgiDir . '/fh_xmlparser.php');
} else {
      exit();
}

$conf          = "";
$fieldsDBArray = "";
$errorLabel    = "";
$errorField    = "";
$firstLineUD   = "";
$MCompIdent    = "";

$commonPath    = $cgiDir . "/fh_common.php";
$formName      = "form".$nof_componentId;



if (NOF_fileExists($commonPath)) {
      require_once($commonPath);
} else {
      exit();
}
?>
<div style="width:500px;" id="errorshow"></div>
<script type="text/javascript">
    function fh_changePage(op) {
    	document.<?=$formName?>.fhpageoperation.value = op;
    	document.<?=$formName?>.submit();

    }

    function fh_changeOp(op) {
    	document.<?=$formName?>.fhoperation.value = op;
    	//document.<?=$formName?>.submit();

    }
</script>
<?


//read the XML property file
$conf = fhParseXmlFile($xmlPropertyFile);

if(!isset($componentid)) {
      NOF_throwError(800,array("{1}"=>"componentid","{2}"=>$nof_suiteName));
      exit();
}


$MCompIdent = "manager" . "."  . $componentid . ".";

$tmp_DBname = $conf[$MCompIdent . "dbPath"];

//TODO: have to get this from xml or post
$FHID = getFHID($conf[$MCompIdent . "dbPath"]);
if($FHID == -1) {
    NOF_throwError(602,array("{1}"=>$nof_suiteName,"{2}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"])));
    exit();
}
$FHCompIdent = "formshandler." . $FHID . ".";



//add the cgi-bin path ahead of the DB file name if its a pure file name without whole path
//(when the file path is just a name its asssumed that its in cgi-bin)
if( !preg_match("/\//" , $conf[$MCompIdent . "dbPath"]) && !preg_match('/(\\\\)/' , $conf[$MCompIdent . "dbPath"])   ) {
      $conf[$MCompIdent . "dbPath"]=$cgiDir . "/" . $conf[$MCompIdent . "dbPath"];
}



//get the names of fields present in the database
getExpectedDBFields($FHCompIdent);



if ($conf[$MCompIdent . "writeToDB"]!="false") {
    if (!file_exists(dirname($conf[$MCompIdent . "dbPath"]))) {
        NOF_throwError(540,array("{1}"=>dirname(NOF_mapPath($conf[$MCompIdent . "dbPath"])),"{2}"=>NOF_mappath(getcwd())));
    }

    if (!is_writable(dirname($conf[$MCompIdent . "dbPath"]))) {
        NOF_throwError(541,array("{1}"=>dirname(NOF_mapPath($conf[$MCompIdent . "dbPath"])),"{2}"=>NOF_mappath(getcwd())));
    }
    //if DB does not exist or if first line of dbColumns.db(column headings
    //does not match first line of DB meaning that
    //the file has to be created/overwritten with new columns from dbColumns.db
    if( !checkIfDBmatch($MCompIdent) ) {
        //open DB file for writing
        if (!$FILE = @fopen($conf[$MCompIdent . "dbPath"], 'wb')) {
            NOF_throwError(500,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
        }

        //write first line of UD file, to the DB file
        if (!fputs($FILE, "$firstLineUD\n")) {
            NOF_throwError(502,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
        }
        fclose($FILE);
    }
}

if ( GetPostVariable("fhpageoperation")=='' ) {
      SetPostVariable("fhpageoperation", "");
}

$fh_var["begin"] = 1;
if ( GetPostVariable("begin")!='' ) {
      $fh_var["begin"] = GetPostVariable("begin");
}

if ( GetPostVariable("fhpageoperation")=='Next' ) {
      $fh_var["begin"] = $fh_var["begin"] + 10;
} elseif( GetPostVariable("fhpageoperation")=='Prev') {
      $fh_var["begin"] = $fh_var["begin"] - 10;
} elseif( GetPostVariable("fhpageoperation")=='First' ) {
      $fh_var["begin"] = 1;
} elseif( GetPostVariable("fhpageoperation")=='Last' ) {
      $fh_var["begin"] = -100;
      fhDisplayLastEntriesList();
}


//if no operation was passed (meaning page is loaded the first time)
if( GetPostVariable("fhoperation") == '' )  {
      //display upto 10 entries beginning from 1
      if($fh_var["begin"] > -90){ fhDisplayEntriesList($fh_var["begin"]); }
} elseif( GetPostVariable("fhoperation")=='delete' && GetPostVariable("begin")!='' ) { //if remove entry button was pressed
      //the entry selected in the radio group
      if( GetPostVariable("radiogroup")!='' ) {
            //get the numer of the entry that needs to removed
            $entryToBeRemoved = GetPostVariable("radiogroup");
            //remove the entry
            fhRemoveEntry($entryToBeRemoved);
      }
      //display upto 10 entries from $_POST["begin"]
      fhDisplayEntriesList(GetPostVariable("begin"));
} elseif ( GetPostVariable("fhoperation")=='view' && GetPostVariable("begin")!='' ) { //if View button was pressed
      //the user name selected in the radio group
      if( GetPostVariable("radiogroup")!='' ) {
            //get the numer of the entry that needs to removed
            $entryToBeViewed = GetPostVariable("radiogroup");
            //display details of the selected user
            fhDisplayEntryDetails($entryToBeViewed, GetPostVariable("begin"));
      }
} elseif ( GetPostVariable("fhoperation")=='edit' && GetPostVariable("begin")!='' ) { //if Edit button was pressed
      //the user name selected in the radio group
      if( GetPostVariable("radiogroup")!='' ) {
            //get the numer of the entry that needs to removed
            $entryToBeEdited = GetPostVariable("radiogroup");
            //display details of the selected user
            fhDisplayEditEntry($entryToBeEdited, GetPostVariable("begin"));
      }
} elseif ( GetPostVariable("fhoperation")=='add' && GetPostVariable("begin")!='' ) { //if add user button was pressed
      //display add user form
      fhDisplayAddEntry(GetPostVariable("begin"));
} elseif( GetPostVariable("fhoperation")=='back' && GetPostVariable("begin")!='' ) { //if back to entries list was pressed
      //display the list of entries from "begin"
      fhDisplayEntriesList(GetPostVariable("begin"));
} elseif( GetPostVariable("fhoperation")=='reset' && GetPostVariable("entrytobeedited")!='' ) { //if Reset was pressed
      //display the edit form
      fhDisplayEditEntry(GetPostVariable("entrytobeedited"), GetPostVariable("begin"));
} elseif ( GetPostVariable("fhoperation")=='saveentry' && GetPostVariable("entrytobeedited")!='' ) { //if save entry button was pressed
      $errorFlag=FALSE;
      //if the required parameters were unfilled
      if ( fhCheckIfRequiredUnfilled() ) {
            //display the error
            fhDisplayError($errorLabel,$errorField);
            //display the edit form filled with previous values
            fhDisplayEditEntryError(GetPostVariable("entrytobeedited"), GetPostVariable("begin"), $errorField);
            exit();
      }

      //if fields are not within their length limitations
      if( fhCheckIfFieldsLengthInvalid() ) {
            $errorFlag=TRUE; //set error flag to true
      }

      //on error
      if( $errorFlag ) {
            //display the error
            fhDisplayError($errorLabel,$errorField);
            //display the edit form with previous values filled
            fhDisplayEditEntryError(GetPostVariable("entrytobeedited"),GetPostVariable("begin"), $errorField);
            exit();
      } else {  //if no errors were encountered in user input
            //change the entry details
            fhChangeEntry(GetPostVariable("entrytobeedited"));
            //display success message
            fhDisplaySuccessMessage($GLOBALS['nof_resources']->get('FH.Manager.Text.EditSuccess'));
            //display the edit entry form with the changed details
            fhDisplayEditEntry(GetPostVariable("entrytobeedited"), GetPostVariable("begin"));
            exit();
      }
} elseif ( GetPostVariable("fhoperation")=='addentry' && GetPostVariable("begin")!='' ) {
      //if add user button was pressed
      $errorFlag=FALSE;

      //if the required parameters were unfilled
      if ( fhCheckIfRequiredUnfilled() ) {
            //display error at the top
            fhDisplayError($errorLabel,$errorField);
            //display the add form with previous values filled
            fhDisplayAddEntryError(GetPostVariable("begin"), $errorField);
            exit();
      }
      //if fields are not within their length limitations
      if( fhCheckIfFieldsLengthInvalid() ) {
            $errorFlag=TRUE; //set error flag
      }

      //if uploaded files had any errors
      if( checkIfUploadedFilesInvalid($FHCompIdent) ) {
            $errorFlag=TRUE; //set error flag to true
      }
      //on error
      if( $errorFlag ) {
            //display error at the top
            fhDisplayError($errorLabel,$errorField);
            //display the add form with the previous values filled
            fhDisplayAddEntryError(GetPostVariable("begin"), $errorField);
            exit();
      } else {
            //if no errors were encountered in user input
            //store the uploaded file in the directory specified in the property file
            getUploadedFile($MCompIdent);
            //add the new entry to the end of the DB file
            fhInsertInDB();
            //display the successmessage
            fhDisplaySuccessMessage($GLOBALS['nof_resources']->get('FH.Manager.Text.AddSuccess'));
            //display the last entries of the DB
            fhDisplayLastEntriesList();
            exit();
      }
}




//function to remove a particular entry from the DB
function fhRemoveEntry($entryToBeRemoved) {
      global $conf,$cgiDir,$MCompIdent;

      //get the db in an array
      if(!$lines = @file($conf[$MCompIdent . "dbPath"])) {
            NOF_throwError(501,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }

      //open db file for writing
      if (!$FILE = @fopen($conf[$MCompIdent. "dbPath"], 'wb')) {
            NOF_throwError(502,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }

      //dump into the file all lines except where the user to be removed was found
      for( $i=0; $i<count($lines); $i++ ) {
            if($i!=$entryToBeRemoved) {
                  if (!fputs($FILE, $lines[$i]) ) {
                        NOF_throwError(502,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
                        exit();
                  }
            }
      }
}


//function to display upto 10 entries from $begin
function fhDisplayEntriesList($begin) {
      global $conf,$fieldsDBArray,$MCompIdent,$formName;

      //get the whole database in an array
      if(!$lines = @file($conf[$MCompIdent. "dbPath"])) {
            NOF_throwError(501,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }

      if ($begin >= count($lines)) {
            $begin = $begin -10;
      }

      if ($begin < 1 ) {
            $begin = 1;
      }

      //begin form
      echo "<FORM NAME=\"$formName\" TARGET=\"_self\" ACTION=\"" . GetServerVariable('PHP_SELF') . "\"  METHOD=\"POST\">" . "\r\n";

      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"begin\" VALUE=\"" . $begin . "\">" . "\r\n";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"fhpageoperation\" value=\"\">" . "\r\n";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"fhoperation\" value=\"\">" . "\r\n";

      echo "<TABLE BGCOLOR=\"#C3C3C3\" BORDER=\"1\" CELLPADDING=\"2\" CELLSPACING=\"5\">" . "\r\n";

      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD ALIGN=\"center\" COLSPAN=\"6\">" . "\r\n";
      echo "<b>".$GLOBALS['nof_resources']->get('FH.Manager.Text.BrowseEntries')."</b>" . "\r\n";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";

      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<td><b>&nbsp;</b></TD>" . "\r\n";
      //print the column names
      $noofcols=min(5,count($fieldsDBArray));
      for($i=0;$i<$noofcols;$i++) {
            echo "<td ALIGN=\"center\"><b>" . ucfirst(strtolower($fieldsDBArray[$i])) . "</b></TD>" . "\r\n";
      }
      echo "</TR>" . "\r\n";
      $end = min(count($lines),$begin+10);
      //display 10 entries from beginning
      for( $i=$begin; $i< $end; $i++ ) {
            echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
            echo "<TD>";
            //display the radio button
            if($i==$begin) { //if its the first row
                  //make the radio button be checked
                  echo "<input type=\"radio\" name=\"radiogroup\" value=\"" . $i ."\" CHECKED>" . "\r\n";
            } else {  //if not the first user
                  //make the radio button unchecked
                  echo "<input type=\"radio\" name=\"radiogroup\" value=\"" . $i ."\">" . "\r\n";
            }
            echo "</TD>" . "\r\n";
            //display each column entry for the row
                  $noofcols=min(5,count($fieldsDBArray));
                  for($j=0;$j<$noofcols;$j++) {
                        $field = getField(getColumnPos($fieldsDBArray[$j]),$lines[$i]);
                        echo "<TD align=\"left\">";
                        echo fhHrefSurroundIfFile($fieldsDBArray[$j],$field);
                        echo "</TD>" . "\r\n";
                  }
                  echo "</TR>" . "\r\n";
      }

      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<td ALIGN=\"CENTER\" COLSPAN=\"6\">" . "\r\n";
      //add entry button
      if ($conf[$MCompIdent . "writeToDB"]=="false") {
            $canedit = "disabled";
      } else {
            $canedit = "";
      }
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('add');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.add')."\" $canedit>&nbsp;&nbsp;" . "\r\n";
      if($end<=1) { //if there are not entries in the database
            //make the view details, edit entry and remove entry buttons disabled
            echo "<INPUT TYPE=\"BUTTON\" DISABLED VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.edit')."\" $canedit>&nbsp;" . "\r\n";
            echo "<INPUT TYPE=\"BUTTON\" DISABLED VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.view')."\">&nbsp;&nbsp;" . "\r\n";
            echo "<INPUT TYPE=\"BUTTON\" DISABLED VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.delete')."\" $canedit>" . "\r\n";
      } else {  // if there are entries in the database
            //make the view details, edit entry and remove entry buttons as enabled
            echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('edit');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.edit')."\" $canedit>&nbsp;&nbsp;" . "\r\n";
            echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('view');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.view')."\">&nbsp;&nbsp;" . "\r\n";
            echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('delete');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.delete')."\" $canedit>" . "\r\n";
      }
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";
      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD ALIGN=\"center\" COLSPAN=\"6\">" . "\r\n";
      echo "<TABLE BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"2\" WIDTH=\"90%\">" . "\r\n";
      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD ALIGN=\"left\">";
      if($begin > 1) {
            echo "<A HREF=\"#\" target=\"_self\" ONCLICK=\"fh_changePage('First');\">".$GLOBALS['nof_resources']->get('FH.Manager.Link.FirstPage')."</A>&nbsp;&nbsp;";
            echo "<A HREF=\"#\" target=\"_self\" ONCLICK=\"fh_changePage('Prev');\">".$GLOBALS['nof_resources']->get('FH.Manager.Link.PreviousPage')."</A>";
      } else {
            echo $GLOBALS['nof_resources']->get('FH.Manager.Link.FirstPage')."&nbsp;&nbsp;";
            echo $GLOBALS['nof_resources']->get('FH.Manager.Link.PreviousPage');
      }
      echo "</TD>" . "\r\n";
      echo "<TD ALIGN=\"right\">";

      if($end < count($lines)) {
            echo "<A HREF=\"#\" target=\"_self\" ONCLICK=\"fh_changePage('Next');\">".$GLOBALS['nof_resources']->get('FH.Manager.Link.NextPage')."</A>&nbsp;&nbsp;" ;
            echo "<A HREF=\"#\" target=\"_self\" ONCLICK=\"fh_changePage('Last');\">".$GLOBALS['nof_resources']->get('FH.Manager.Link.LastPage')."</A>" ;
      } else {
            echo $GLOBALS['nof_resources']->get('FH.Manager.Link.NextPage')."&nbsp;&nbsp;";
            echo $GLOBALS['nof_resources']->get('FH.Manager.Link.LastPage');
      }

      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";
      echo "</TABLE>" . "\r\n";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";
      echo "</TABLE>" . "\r\n";
      echo "</FORM>" . "\r\n";
}


//function to display the last page of entries
function fhDisplayLastEntriesList() {
      global $conf,$fieldsDBArray,$MCompIdent,$formName;
      //get the whole database in an array
      if(!$lines = @file($conf[$MCompIdent . "dbPath"])) {
            NOF_throwError(501,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }
      //set begin
      $begin=count($lines) - (count($lines)-1)%10;
      if ($begin < 1 ) {
            $begin = 1;
      }
      if ($begin >= count($lines)) {
            $begin = $begin -10;
      }

      //begin form
      echo "<FORM NAME=\"$formName\" TARGET=\"_self\" ACTION=\"" . GetServerVariable('PHP_SELF') . "\"  METHOD=\"POST\">" . "\r\n";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"fhpageoperation\" value=\"\">" . "\r\n";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"begin\" VALUE=\"" . $begin . "\">" . "\r\n";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"fhoperation\" value=\"\">" . "\r\n";

      echo "<TABLE BGCOLOR=\"#C3C3C3\" BORDER=\"1\" CELLPADDING=\"2\" CELLSPACING=\"5\">" . "\r\n";
      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD ALIGN=\"center\" COLSPAN=\"6\">" . "\r\n";
      echo "<b>".$GLOBALS['nof_resources']->get('FH.Manager.Text.BrowseEntries')."</b>" . "\r\n";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";

      echo "<tr bgcolor=\"#E3E3E3\">" . "\r\n";
      echo "<td><b>&nbsp;</b></td>" . "\r\n";
      //print the column names
            $noofcols=min(5,count($fieldsDBArray));
            for($i=0;$i<$noofcols;$i++) {
                  echo "<td align=\"center\"><b>" . ucfirst(strtolower($fieldsDBArray[$i])) . "</b></td>" . "\r\n";
            }

            $end = min(count($lines),$begin+10);
            //display 10 entries from beginning
            for( $i=$begin; $i< $end; $i++ ) {

                  echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
                  echo "<TD>";
                  if($i==$begin) {
                        echo "<input type=\"radio\" name=\"radiogroup\" value=\"" . $i ."\" CHECKED>" . "\r\n";
                  } else {
                        echo "<input type=\"radio\" name=\"radiogroup\" value=\"" . $i ."\">" . "\r\n";
                  }
                  echo "</TD>" . "\r\n";

                  $noofcols=min(5,count($fieldsDBArray));
                  for($j=0;$j<$noofcols;$j++) {
                        $field = getField(getColumnPos($fieldsDBArray[$j]),$lines[$i]);
                        echo "<TD align=\"left\">";
                        echo fhHrefSurroundIfFile($fieldsDBArray[$j],$field);
                        echo "</TD>";
                  }

                  echo "</TR>" . "\r\n";
            }

            echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
            echo "<td ALIGN=\"CENTER\" COLSPAN=\"6\">";
            //add entry button
            echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('add');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.add')."\">&nbsp;&nbsp;";
            if($end<=1) { //if there are not entries in the database
                  //make the view details, edit entry and remove entry buttons disabled
                  echo "<INPUT TYPE=\"BUTTON\" DISABLED VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.edit')."\">&nbsp;";
                  echo "<INPUT TYPE=\"BUTTON\" DISABLED VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.view')."\">&nbsp;&nbsp;";
                  echo "<INPUT TYPE=\"BUTTON\" DISABLED VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.delete')."\">";
            } else {  // if there are entries in the database
                  //make the view details, edit entry and remove entry buttons as enabled
                  echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('edit');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.edit')."\">&nbsp;&nbsp;";
                  echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('view');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.view')."\">&nbsp;&nbsp;";
                  echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('delete');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.delete')."\">";
            }
            echo "</TD>" . "\r\n";
            echo "</TR>" . "\r\n";

            echo "<TR BGCOLOR=\"#E3E3E3\">";
            echo "<TD ALIGN=\"center\" COLSPAN=\"6\">";
            echo "<TABLE BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"2\" WIDTH=\"90%\">";
            echo "<TR BGCOLOR=\"#E3E3E3\">";
            echo "<TD ALIGN=\"left\">";
            if($begin > 1) {
                  echo "<A HREF=\"#\" target=\"_self\" ONCLICK=\"fh_changePage('First');\">".$GLOBALS['nof_resources']->get('FH.Manager.Link.FirstPage')."</A>&nbsp;&nbsp;" ;
                  echo "<A HREF=\"#\" target=\"_self\" ONCLICK=\"fh_changePage('Prev');\">".$GLOBALS['nof_resources']->get('FH.Manager.Link.PreviousPage')."</A>" ;
            } else {
                  echo $GLOBALS['nof_resources']->get('FH.Manager.Link.FirstPage')."&nbsp;&nbsp;";
                  echo $GLOBALS['nof_resources']->get('FH.Manager.Link.PreviousPage') ;
            }

            echo "</TD>";
            echo "<TD ALIGN=\"right\">";

            if($end < count($lines)) {
                  echo "<A HREF=\"#\" target=\"_self\" ONCLICK=\"fh_changePage('Next');\">".$GLOBALS['nof_resources']->get('FH.Manager.Link.NextPage')."</A>&nbsp;&nbsp;" ;
                  echo "<A HREF=\"#\" target=\"_self\" ONCLICK=\"fh_changePage('Last');\">".$GLOBALS['nof_resources']->get('FH.Manager.Link.LastPage')."</A>" ;
            } else {
                  echo $GLOBALS['nof_resources']->get('FH.Manager.Link.NextPage')."&nbsp;&nbsp;";
                  echo $GLOBALS['nof_resources']->get('FH.Manager.Link.LastPage');
            }

            echo "</TD>";
            echo "</TR>" . "\r\n";
            echo "</TABLE>" . "\r\n";
            echo "</TD>";
            echo "</TR>" . "\r\n";
            echo "</TABLE>" . "\r\n";
            echo "</FORM>" . "\r\n";
}



//function to display form to edit entry details
function fhDisplayEditEntry($entryToBeEdited,$begin) {

      global $conf,$cgiDir,$fieldsDBArray,$MCompIdent,$formName;

      //get the db in an array
      if(!$lines = @file($conf[$MCompIdent. "dbPath"])) {
            NOF_throwError(501,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }

      //begin form
      echo "<FORM NAME=\"$formName\"   TARGET=\"_self\" ACTION=\"" . GetServerVariable('PHP_SELF') . "\"  METHOD=\"POST\">" . "\r\n";

      //begin table
      echo "<TABLE BGCOLOR=\"#C3C3C3\" BORDER=\"1\" CELLPADDING=\"0\" CELLSPACING=\"5\">" . "\r\n";

      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD ALIGN=\"CENTER\" COLSPAN=\"2\">";
      echo "<b>".$GLOBALS['nof_resources']->get('FH.Manager.Title.editentry')."</b>";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";

      //printout details of the entry
      for($i=0;$i<count($fieldsDBArray);$i++) {
            echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
            echo "<td><b>" . $fieldsDBArray[$i] . "</b></td>";

            echo "<td><INPUT TYPE=\"TEXT\" NAME=\"" . $fieldsDBArray[$i] . "\""
            . "VALUE=\""
            . getField(getColumnPos($fieldsDBArray[$i]),$lines[$entryToBeEdited])
            . "\"></td>";

            echo "</TR>" . "\r\n";
      }

      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD ALIGN=\"center\" COLSPAN=\"2\">";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"fhoperation\" value=\"\">" . "\r\n";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('saveentry');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.saveentry')."\">&nbsp;&nbsp;&nbsp;";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('reset');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.reset')."\">&nbsp;&nbsp;&nbsp;";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('back');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.back')."\">";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";
      echo "</TABLE>" . "\r\n";

      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"begin\" VALUE=\"" . $begin . "\">" . "\r\n";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"entrytobeedited\" VALUE=\"" . $entryToBeEdited . "\">" . "\r\n";
      echo "</FORM>" . "\r\n";

}




//function to display form to view entry details
function fhDisplayEntryDetails($entryToBeViewed,$begin) {

      global $conf,$cgiDir,$fieldsDBArray,$MCompIdent,$formName;

      //get the db in an array
      if(!$lines = @file($conf[$MCompIdent. "dbPath"])) {
            NOF_throwError(501,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }

      //begin form
      echo "<FORM NAME=\"$formName\"   TARGET=\"_self\" ACTION=\"" . GetServerVariable('PHP_SELF') . "\"  METHOD=\"POST\">" . "\r\n";

      //begin table
      echo "<TABLE BGCOLOR=\"#C3C3C3\" BORDER=\"1\" CELLPADDING=\"2\" CELLSPACING=\"5\">" . "\r\n";
      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD ALIGN=\"CENTER\" COLSPAN=\"2\">";
      echo "<b>".$GLOBALS['nof_resources']->get('FH.Manager.Title.viewentry')."</b>";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";

      //printout details of the entry
      for($i=0;$i<count($fieldsDBArray);$i++) {
            echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
            echo "<td><b>" . ucfirst(strtolower($fieldsDBArray[$i])) . "</b></td>" . "\r\n";

            echo "<td>"
            . fhHrefSurroundIfFile($fieldsDBArray[$i],getField(getColumnPos($fieldsDBArray[$i]),$lines[$entryToBeViewed]))
            ."</td>" . "\r\n";

            echo "</TR>" . "\r\n";
      }

      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<td align=\"center\" colspan=\"2\">";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"fhoperation\" value=\"\">" . "\r\n";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('back');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.back')."\"></TD>";
      echo "</TR>" . "\r\n";

      echo "</TABLE>" . "\r\n";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"begin\" VALUE=\"" . $begin . "\">";

      echo "</FORM>" . "\r\n";
}




//function to display form to add an entry to the db
function fhDisplayAddEntry($begin) {

      global $conf,$cgiDir,$fieldsDBArray,$formName;

        //begin form
      echo "<FORM NAME=\"$formName\"   TARGET=\"_self\" ACTION=\"" . GetServerVariable('PHP_SELF') . "\"  METHOD=\"POST\" enctype=\"multipart/form-data\">" . "\r\n";

        //begin table
      echo "<TABLE BGCOLOR=\"#C3C3C3\" BORDER=\"1\" CELLPADDING=\"0\" CELLSPACING=\"5\">" . "\r\n";

      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD ALIGN=\"CENTER\" COLSPAN=\"2\">";
      echo "<b>".$GLOBALS['nof_resources']->get('FH.Manager.Title.addentry')."</b>";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";

      //display the textfields with names same as the columns of db
      for($i=0;$i<count($fieldsDBArray);$i++) {
            echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
            echo "<td><b>" . ucfirst(strtolower($fieldsDBArray[$i])) . "</b></td>" . "\r\n";
            //if its a file field
            if(fhIsFileField($fieldsDBArray[$i])) {
                  echo "<td><INPUT TYPE=\"FILE\" NAME=\""
                   . $fieldsDBArray[$i] . "\""
                   . "VALUE=\"\"></td>";

            } else { //for all other fields
                 echo "<td><INPUT TYPE=\"TEXT\" NAME=\""
                   . $fieldsDBArray[$i] . "\""
                   . "VALUE=\"\"></td>";
            }
            echo "</tr>" . "\r\n";
      }

      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD colspan=\"2\" align=\"center\">";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"fhoperation\" value=\"\">" . "\r\n";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('addentry');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.addentry')."\">&nbsp;&nbsp;&nbsp;";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('back');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.back')."\">";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";

      //end table
      echo "</TABLE>" . "\r\n";

      echo "<INPUT TYPE=HIDDEN NAME=\"begin\" VALUE=\"" . $begin . "\">" . "\r\n";

      echo "</FORM>" . "\r\n";
}


function fhDisplayAddEntryError($begin,$errorField) {
      global $conf,$cgiDir,$fieldsDBArray,$formName;

      $errorFields = explode(",",$errorField);

        //begin form
      echo "<FORM NAME=\"$formName\"   TARGET=\"_self\" ACTION=\"" . GetServerVariable('PHP_SELF') . "\"  METHOD=\"POST\" enctype=\"multipart/form-data\">" . "\r\n";

        //begin table
      echo "<TABLE BGCOLOR=\"#C3C3C3\" BORDER=\"1\" CELLPADDING=\"0\" CELLSPACING=\"5\">" . "\r\n";

      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD ALIGN=\"CENTER\" COLSPAN=\"3\">";
      echo "<b>".$GLOBALS['nof_resources']->get('FH.Manager.Title.addentry')."</b>";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";
      //printout entry details
      for($i=0;$i<count($fieldsDBArray);$i++) {
            echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
            echo "<td><b>" . $fieldsDBArray[$i] . "</b></td>" . "\r\n";
            if(in_array($fieldsDBArray[$i],$errorFields)) {
                  echo "<td><b>&nbsp;<font color=\"red\" >!</font>&nbsp;</b></td>" . "\r\n";
            } else {
                  echo "<td>&nbsp;&nbsp;&nbsp;</td>" . "\r\n";
            }
            if(!fhIsFileField($fieldsDBArray[$i])) {
                 echo "<td><INPUT TYPE=\"TEXT\" NAME=\""
                 . $fieldsDBArray[$i] . "\""
                  . " VALUE=\"" . escapeString(stripslashes(GetPostVariable($fieldsDBArray[$i]))) ."\"></td>";
            } else {
                 echo "<td><INPUT TYPE=\"FILE\" NAME=\"" . $fieldsDBArray[$i] . "\"" . "VALUE=\"\"></td>" . "\r\n";
            }
            echo "</tr>" . "\r\n";
      }

      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD colspan=\"3\" align=\"center\">";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"fhoperation\" value=\"\">" . "\r\n";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('addentry');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.addentry')."\">&nbsp;&nbsp;&nbsp;";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('back');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.back')."\">";
      echo "</TD>";
      echo "</TR>";

      //end table
      echo "</TABLE>" . "\r\n";

      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"begin\" VALUE=\"" . $begin . "\">" . "\r\n";

      echo "</FORM>" . "\r\n";
}




function fhDisplayEditEntryError($entryToBeEdited,$begin,$errorField) {
      global $conf,$cgiDir,$fieldsDBArray,$MCompIdent,$formName;

      $errorFields = explode("'",$errorField);

      //get the db in an array
      if(!$lines = @file($conf[$MCompIdent. "dbPath"])) {
            NOF_throwError(501,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }

      //begin form
      echo "<FORM NAME=\"$formName\" TARGET=\"_self\" ACTION=\"" . GetServerVariable('PHP_SELF') . "\"  METHOD=\"POST\">" . "\r\n";

      //begin table
      echo "<TABLE BGCOLOR=\"#C3C3C3\" BORDER=\"1\" CELLPADDING=\"0\" CELLSPACING=\"5\">" . "\r\n";
      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD ALIGN=\"CENTER\" COLSPAN=\"3\">";
      echo "<b>".$GLOBALS['nof_resources']->get('FH.Manager.Title.editentry')."</b>";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";

      //printout entry details
      for($i=0;$i<count($fieldsDBArray);$i++) {
            echo "<TR BGCOLOR=\"#E3E3E3\">";
            echo "<td><b>" . ucfirst(strtolower($fieldsDBArray[$i])) . "</b></td>" . "\r\n";
            if(in_array($fieldsDBArray[$i],$errorFields)) {
                  echo "<td><b>&nbsp;<font color=\"red\" >!</font>&nbsp;</b></td>" . "\r\n";
            } else{
                  echo "<td>&nbsp;&nbsp;&nbsp;</td>" . "\r\n";
            }
            echo "<td><INPUT TYPE=\"TEXT\" NAME=\""
            . $fieldsDBArray[$i] . "\""
            . "VALUE=\""
            . escapeString(stripslashes(GetPostVariable($fieldsDBArray[$i]))) . "\"></td>" . "\r\n";

            echo "</tr>" . "\r\n";
      }


      echo "<TR BGCOLOR=\"#E3E3E3\">" . "\r\n";
      echo "<TD align=\"center\" colspan=\"3\">";
      echo "<INPUT TYPE=\"HIDDEN\" NAME=\"fhoperation\" value=\"\">" . "\r\n";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('saveentry');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.saveentry')."\">&nbsp;&nbsp;&nbsp;";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('reset');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.reset')."\">&nbsp;&nbsp;&nbsp;";
      echo "<INPUT TYPE=\"BUTTON\" ONCLICK =\"fh_changeOp('back');document.$formName.submit();\" VALUE=\"".$GLOBALS['nof_resources']->get('FH.Manager.Button.back')."\">&nbsp;&nbsp;&nbsp;";
      echo "</TD>" . "\r\n";
      echo "</TR>" . "\r\n";
      echo "</TABLE>" . "\r\n";

      echo "<INPUT TYPE=HIDDEN NAME=\"begin\" VALUE=\"" . $begin . "\">" . "\r\n";
      echo "<INPUT TYPE=HIDDEN NAME=\"entrytobeedited\" VALUE=\"" . $entryToBeEdited . "\">" . "\r\n";
      echo "</FORM>" . "\r\n";
}



//function to change a particular entry in the DB
function fhChangeEntry($entryToBeChanged) {
      global $conf,$fieldsDBArray,$MCompIdent,$formName;

      //get the db in an array
      if(!$lines = @file($conf[$MCompIdent. "dbPath"])) {
            NOF_throwError(501,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }

      //make the changed row entry
      $entry="";
      for($i=0;$i<count($fieldsDBArray);$i++) {
            if($entry=="") {
                  $entry = preprocess(GetPostVariable($fieldsDBArray[$i]));
            } else {
                  $entry = $entry . "," .  preprocess(GetPostVariable($fieldsDBArray[$i]));
            }
      }

      //replace the entry in the DB with the new entry
      $lines[$entryToBeChanged]=$entry . "\n";

      //open db file for writing
      if (!$FILE = @fopen($conf[$MCompIdent. "dbPath"], 'wb')) {
            NOF_throwError(502,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }

      //dump into the db
      for($i=0;$i<count($lines);$i++) {
            if (!fputs($FILE, $lines[$i]) ){
                  NOF_throwError(502,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
                  exit();
            }
      }
      fclose($FILE);
}


//function to see if all required values are filled
function fhCheckIfRequiredUnfilled() {
      global $conf,$FHCompIdent;

      $errorFoundFlag=FALSE;

      $postArr = GetPostVariable('');
      reset($postArr);
      while (list($field, $value) = each ($postArr)) {
            $property = $FHCompIdent . $field . ".errorevent.required.active";
            //if a field is empty and the required property for the field is
            //set in the property file
            if(  preg_match("/^\s*$/",$postArr[$field]) && isset($conf[$property])) {
                  if ($conf[$property]=="true" ) {
                        $label=$FHCompIdent . $field . ".errorevent.required.message" ;
                        clubError($label,$field);
                        $errorFoundFlag=TRUE;
                  }
            }
      }

      $postFiles = GetFileVariable('');
      reset($postFile);
      while(list($field, $value)=each($postFile)) {
            $property = $FHCompIdent . $field . ".errorevent.required.active";
            //if a file field is not filled and the required property for the field is
            //set in the property file
            if( !isset($postFile[$field]['name'])
            || empty($postFile[$field]['name'])
            && isset($conf[$property])) {
                  if ($conf[$property]=="true") {
                        $label=$FHCompIdent . $field . ".errorevent.required.message" ;
                        clubError($label,$field);
                        $errorFoundFlag=TRUE;
                  }

            }
      }

      return $errorFoundFlag;
}


//function to see if the field lengths are within the limits
function fhCheckIfFieldsLengthInvalid() {
      global $conf,$FHCompIdent;

      $errorFoundFlag=FALSE;

      $postArr = GetPostVariable('');
      reset($postArr);
      while( list($field,$value)= each($postArr) ) {

            $shortErrorEventActiveProperty = $FHCompIdent . $field . ".errorevent.short.active";
            //if the short property for the field is set
            if( $postArr[$field]!="" &&
            isset($conf[$shortErrorEventActiveProperty]) ){
                  if ($conf[$shortErrorEventActiveProperty]=="true" ){
                        $minimumLengthProperty = $FHCompIdent . $field . ".errorevent.short.minimumlength";
                        //if the length of the field is lesser than the limit set in
                        //the property file
                        if( preg_match("/^[-+]{0,1}\d+$/" ,$conf[$minimumLengthProperty] ) ) {
                              if( strlen($postArr[$field]) < $conf[$minimumLengthProperty] ) {
                                    clubError($FHCompIdent . $field . ".errorevent.short.message", $field);
                                    $errorFoundFlag=TRUE;
                              }
                        }
                  }
            }

            $longErrorEventActiveProperty = $FHCompIdent . $field . ".errorevent.long.active";
            //if the long property for the field is set
            if(!isset($conf[$longErrorEventActiveProperty])) {
                  $conf[$longErrorEventActiveProperty] = "false";
            }
            if($conf[$longErrorEventActiveProperty]=="true"){
                  $maximumLengthProperty = $FHCompIdent . $field . ".errorevent.long.maximumlength";
                  //if the length of the field is greater than the limit set in
                  //the property file
                  if( preg_match("/^\+{0,1}\d+$/" ,$conf[$maximumLengthProperty] ) ) {
                        if( strlen($postArr[$field]) > $conf[$maximumLengthProperty] ) {
                              clubError($FHCompIdent . $field . ".errorevent.long.message", $field);
                              $errorFoundFlag=TRUE;
                        }
                  }
            }
      }

      return $errorFoundFlag;
}


//display error message
function fhDisplayError($errorLabel,$errorField) {
      global $conf;
      //get the error labels and associated fields in arrays
      $errorLabel = explode(",",$errorLabel);
      //for each error
      echo "<TABLE BORDER=\"0\" WIDTH=\"100%\">" . "\r\n";
      for($i=0;$i< count($errorLabel);$i++) {
            $property = $errorLabel[$i];
            echo "<TR><TD NOWRAP><FONT COLOR=\"red\"><li>" . stripslashes($conf[$property]) . "</li></FONT></TD></TR>" . "\r\n";
      }
      echo "</TABLE>" . "\r\n";
}


//display success message
function fhDisplaySuccessMessage($messg) {
      echo "<TABLE BORDER=\"0\"  WIDTH=\"100%\">" . "\r\n";
      echo "<TR><TD NOWRAP><FONT COLOR=\"blue\">"  . stripslashes($messg) . "</FONT></TD></TR>" . "\r\n";
      echo "</TABLE>" . "\r\n";
}



//function to check if a field is a file type
//and if so make it a link
function fhHrefSurroundIfFile($fieldName,$fieldValue) {
      global $conf,$cgiDir,$MCompIdent;

      //if the fieldvalue is empty return without doing much
      if(empty($fieldValue)) {
            return $fieldValue;
      }

      //if this field is indeed a file type
      if(fhIsFileField($fieldName)) {
            //if this upload directory specified by the user is not an absolute path
            if(!preg_match("/^[a-z]\:/i",$conf[$MCompIdent. "dbFileUploadDir"])
            &&!preg_match("/^\//",$conf[$MCompIdent. "dbFileUploadDir"]) ) {
                  //get the file path relative to the current page
                  $filePath = $cgiDir  ."/"  //this is how we get to the cgi-bin directory from current page
                  . $conf[$MCompIdent. "dbFileUploadDir"]  //this is how we get to upload directory from cgi-bin
                  . "/" . $fieldValue; //finally the filename
                  if(file_exists($filePath)) {
                        //surround by href tags, file preview available only if path is relative to page
                        $filePathHref = "<A HREF=\"javascript:void(0)\" onclick=\"window.open('". $filePath ."','_blank','width=600,height=500,menubar=no,toolbar=no,scrollbars=yes')\">" . basename($fieldValue) . "</A>";
                        return $filePathHref;
                  } else {
                        $filePathHref = "<A HREF=\"javascript:void(0)\" onclick=\"alert('".addslashes(html_entity_decode($GLOBALS['nof_resources']->get('FH.Manager.Text.FileDoesNotExist',array("{1}"=>$filePath))))."');\">" . basename($fieldValue) . "</A>";
                        return $filePathHref;

                  }

            } else {  //if upload directory specified by user is an absolute path
                  //no surrounding by href tag will take place
                  //cause absolute path may not be in the
                  //public_html directory.
                  return $fieldValue;
            }

      } else { //if field was not of type file return field value as it is
            return $fieldValue;
      }

}


//function that takes the upload dir and
//filename and returns the upload path
//also it changes the name of the file in
//the global file array
function fhSetUploadFilePath($dbFileUploadDir,$fileName,$key) {
      global $conf,$cgiDir;

      //if no upload directory was specified then
      //make it the cgi-bin directory
      $dbFileUploadDir= trim($dbFileUploadDir);

      if(!preg_match("/^[a-z]\:/i",$dbFileUploadDir) && !preg_match("/^\//",$dbFileUploadDir)) {
            //if this upload directory specified by the user
            //is a relative path (relative to cgi-bin)
            //make it relative to the page
            $dbFileUploadDir = $cgiDir . "/" . $dbFileUploadDir;
      }

      $uploadFilePath = $dbFileUploadDir . "/" . $fileName;

      $i=1;
      while(file_exists($uploadFilePath)) {
            $fileNameArr =  explode("." , $fileName);
            if(isset($fileNameArr[1])) {
                  $newFileName = $fileNameArr[0] . $i . "." . $fileNameArr[1];
            } else {
                  $newFileName = $fileNameArr[0] . $i;
            }
            $uploadFilePath = $dbFileUploadDir . "/" . $newFileName;
            $postFile = GetFileVariable($key);
            $postFile['name'] = $newFileName;
            $i++;
      }

      return $uploadFilePath;
}



//function that returns true only if a field is a file type
function fhIsFileField($fieldName) {
      global $conf,$FHCompIdent;

      //this property exists only for file fields in the property file
      $property=$FHCompIdent . $fieldName . ".errorevent.invalidextension.active" ;

      //if the property is set then return true
      return isset($conf[$property]);
}



//insert the field values in the DB
function fhInsertInDB() {
      global $conf,$requiredForPassRetv,$fieldsDBArray,$firstLineUD,$MCompIdent;

      //for every expected field in the DB get its value from post request
      for($i=0;$i<count($fieldsDBArray);$i++) {
            //check if the input is of type file
            $postFile = '';
            if ( GetFileVariable($fieldsDBArray[$i])!='' ) { $postFile = GetFileVariable($fieldsDBArray[$i]); }
            if( $postFile != '' ) {
                  //if the input was left blank
                  if ( empty($postFile['name']) ) {
                        $entryAppend = "";
                  } else {
                        //just put the name of the file in the database
                        $entryAppend = $postFile['name'];
                  }
                  if(!isset($entry)) {
                        $entry = '"' . $entryAppend . '"';
                  } else {
                        $entry = $entry . "," .  '"' . $entryAppend . '"';
                  }
            } else {
                  //if input is not of type file
                  $entryAppend = GetPostVariable($fieldsDBArray[$i]);

                  if(!isset($entry)) {
                        $entry = preprocess($entryAppend);
                  } else {
                        $entry = $entry . "," .  preprocess($entryAppend);
                  }

            }

      }

      //open DB file for appending
      if (!$FILE = @fopen($conf[$MCompIdent. "dbPath"], 'ab')) {
            NOF_throwError(502,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }

      //append entry to the file
      if (!fputs($FILE, "$entry\n")) {
            NOF_throwError(502,array("{1}"=>NOF_mapPath($conf[$MCompIdent . "dbPath"]),"{2}"=>NOF_mapPath(dirname($conf[$MCompIdent . "dbPath"]))));
            exit();
      }
      //close file handle
      fclose($FILE);

}



function getFHID($managerDBPath) {
      global $conf;

      $cmp = -1;
      reset($conf);

      while (list($key,$value) = each($conf)) {
            if(preg_match("/^formshandler\.(\d+)\.dbPath$/",$key,$match)) {
                  if($value == $managerDBPath) {
                        $cmp = $match[1];
                        //return $match[1];
                  }
            }
      }

      return $cmp;
}


?>
