<?php

function clubError($error_label,$error_field) {
      global $errorField,$errorLabel;

      if ($errorLabel == "") {
            $errorLabel = $error_label;
      } else {
            $parts = array($errorLabel, $error_label);
            $errorLabel = implode(",", $parts);
      }

      if ($errorField == "") {
            $errorField = $error_field;
      } else {
            $parts = array($errorField, $error_field);
            $errorField = implode(",", $parts);
      }
}


function makeRelativeToCgiBin($path) {
      $path = preg_replace("/^(\.\.\/){1,}/", "", $path);
      $path = preg_replace("/^(\.\/)/", "", $path);
      $path = "../" . $path;

      return $path;
}


function readEmailTemplate($filePath, $kind, $compIdent='') {
      global $conf;

      if(!$lines = @file($filePath)) {
            NOF_throwError(110,array("{1}"=>NOF_mapPath($filePath),"{2}"=>$nof_suiteName));
            exit();
      }
      $flag = 1;
      for($i=0; $i<count($lines); $i++) {
            if( preg_match("/\=/",$lines[$i]) && !preg_match("/^\s*\#/", $lines[$i]) ){
                  list($name,$value) = split("=",$lines[$i]);
                  $name = trim($name);
                  $name = $kind . $name;
                  $value = trim($value);
                  $conf[$name] = $value;
                  $flag = 0;
            }
            if($flag == 1){
                  $conf[$name] .= $lines[$i];
            }
            if($flag == 0){
                  $flag = 1;
            }
      }
}


function readPropConfFile($filePath, $kind) {
      global $conf;

      if(!$lines = @file($filePath)) {
            NOF_throwError(103,array("{1}"=>NOF_mapPath($filePath),"{2}"=>$nof_suiteName));
            exit;
      }

      for($i=0; $i<count($lines); $i++) {
            if( preg_match("/\=/",$lines[$i]) && !preg_match("/^\s*\#/", $lines[$i]) ){
                  list($name,$value) = split("=",$lines[$i]);
                  $name = trim($name);
                  $name = $kind . $name;
                  $value = trim($value);
                  $conf[$name] = $value;
            }
      }
}


function checkIfEmailInvalid($emailAddy, $compIdent) {
      global $conf;

      if( !preg_match("/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/", $emailAddy) ) {
            clubError($compIdent . "email.errorevent.invalidemail.message","email");
            return TRUE;
      }

      return FALSE;
}





function checkIfDBmatch($compIdent) {
      global $conf,$firstLineUD;
      if( !file_exists($conf[$compIdent . "dbPath"]) ) {
            return FALSE;
      }
      if ($lines = @file($conf[$compIdent . "dbPath"])) {
            if(trim($lines[0]) != trim($firstLineUD)) {
                  return FALSE;
            }
            return TRUE;
      } else {
            NOF_throwError(501,array("{1}"=>NOF_mapPath($conf[$compIdent . "dbPath"]),"{1}"=>NOF_mapPath(dirname($conf[$compIdent . "dbPath"]))));
            exit();
      }
}




function preprocess($arg) {
      $arg = trim($arg);
      $arg = preg_replace("/\s/"," ",$arg);
      $arg = str_replace('\"', "", $arg);
      $arg = preg_replace('/(\\\\)/', "" , $arg);
      $arg = '"' . $arg . '"';
      return $arg;
}


function getExpectedDBFields($compIdent) {
      global $conf,$fieldsDBArray,$firstLineUD;
      $firstLineUD = trim($conf[$compIdent . "dbColumn"]);
      $fieldsDBArray = explode("," , $firstLineUD);
}


function getColumnPos($fieldName) {
      global $fieldsDBArray;

      $pos = -1;
      $fieldNameUQ = preg_quote($fieldName);

      for ($i=0; $i<count($fieldsDBArray); $i++) {
            if( preg_match("/^$fieldNameUQ$/i", $fieldsDBArray[$i]) ){
                  $pos = $i;
                  return $pos;
            }
      }

      return $pos;
}


function getField($colNum, $rowData) {
      $userDetailsArray = split( "\"\,\"", $rowData );
      return cleanField($userDetailsArray[$colNum]);
}


function cleanDBField($field) {
      $field = preg_replace("/\"/" , "" , $field);
      $field = trim($field);
      return $field;
}

function cleanField($field) {
      $field = preg_replace("/\"/" , "" , $field);
      $field = preg_replace('/(\\\\)/', "" , $field);
      $field = trim($field);
      return $field;
}

function escapeString($str) {
      $str = str_replace('"' , '&quot;' , $str);
      $str = preg_replace('/\r\n/' , '\n' , $str);

      return $str;
}

function checkIfUploadedFilesInvalid($compIdent) {
      global $conf;
      $postFiles = GetFileVariable('');
      reset($postFiles);
      $errorFoundFlag = FALSE;
      while(list($key,) = each($postFiles)) {
            $largeErrorEventActiveProperty = $compIdent . $key . ".errorevent.filetoolarge.active";
            $maxSizeProperty = $compIdent . $key . ".errorevent.filetoolarge.maximumsize";
            if($conf[$largeErrorEventActiveProperty] == "true" &&
            !empty($postFiles[$key]['name']) &&
            preg_match("/\-?[0-9]\.?[0-9]*/",$conf[$maxSizeProperty]) &&
            $postFiles[$key]['size']  > $conf[$maxSizeProperty] * 1000 ) {
                  $label = $compIdent . $key . ".errorevent.filetoolarge.message" ;
                  clubError($label,$key);
                  $errorFoundFlag = TRUE;
            }

            //check if a disallowed extension is eing uploaded
            $extensionErrorEventActiveProperty = $compIdent . $key . ".errorevent.invalidextension.active";
            $allowedExtensionsProperty = $compIdent . $key . ".errorevent.invalidextension.fileextension";
            if($conf[$extensionErrorEventActiveProperty] == "true" &&
            !empty($conf[$allowedExtensionsProperty]) &&
            !empty($postFiles[$key]['name']) ) {
                  $allowedExtensions = explode ( "|" , $conf[$allowedExtensionsProperty]);
                  //get the extension of uploaded file
                  $pathInfo = pathinfo($postFiles[$key]['name']);
                  $extension = $pathInfo["extension"];
                  //check if the uploaded file extension is in the allowed extensions list
                  if(!in_array($extension,$allowedExtensions) ) {
                        $label = $compIdent . $key . ".errorevent.invalidextension.message" ;
                        clubError($label,$key);
                        $errorFoundFlag = TRUE;
                  }

            }
            if( !empty($postFiles[$key]['name']) &&
            $postFiles[$key]['size'] <=0 ) {
                  $label = $compIdent . $key . ".errorevent.invalidfilename.message" ;
                  clubError($label, $key);
                  $errorFoundFlag = TRUE;
            }

      }

      return $errorFoundFlag;
}


function getUploadedFile($compIdent) {
      global $conf, $cgiDir;

      //if user left the upload directory empty then
      //lets assume he meant a "." meaning cgi-bin directory
      if(empty($conf[$compIdent. "dbFileUploadDir"])) {
            return;
      }

        if ($cgiDir!="") {
            $mycgiDir = $cgiDir . "/";
        } else {
            $mycgiDir = "";
        }
      //reset the $_FILES array
      $postFiles = GetFileVariable('');
      reset($postFiles);

      if (file_exists($mycgiDir . $conf[$compIdent. "dbFileUploadDir"])) {
            //go through the uploaded files array
            foreach($postFiles as $key=>$value) {
                  //transfer the file from the temporary directory to the upload directory
                  //specified in the property file
                  if(!empty($value['name'])) {
                        //alter the file name in case a file with the same name exists
                        $uploadFilePath = fhSetUploadFilePath($conf[$compIdent. "dbFileUploadDir"],$value['name'],$key);
                        if (file_exists($value['tmp_name'])) {
                              if(!@move_uploaded_file($value['tmp_name'],$uploadFilePath)) {
                                    NOF_throwError(702,array("{1}"=>NOF_mapPath($uploadFilePath),"{2}"=>NOF_mapPath($mycgiDir . $conf[$compIdent. "dbFileUploadDir"])));
                                    exit();
                              } else {
                                    chmod($uploadFilePath,0666);
                              }
                        } else {
                              NOF_throwError(302);
                              exit();
                        }
                  }
            }
      } else {
            NOF_throwError(701,array("{1}"=>NOF_mapPath($mycgiDir . $conf[$compIdent. "dbFileUploadDir"]),"{2}"=>NOF_mappath(getcwd())));
            exit();
      }
}

?>
