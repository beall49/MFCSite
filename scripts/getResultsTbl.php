<?php
	
	$con = mysqli_connect('localhost','mante3_rbeall','3VTMx9@!8EZs','mante3_team_info');
	
	$sql = "SELECT
			T.TEAM as TEAM,
			SURVEY_DATE,
			GAME_DATE,
			TXT	
		FROM
			TBL_REF_SURVEY S
		LEFT JOIN (
			SELECT
				CONCAT(YEAR, ' ', ( CASE WHEN ( GENDER = 1 ) THEN 'Boys' ELSE 'Girls' END ), ' ', 'U', 
							  ( CASE WHEN month(curdate()) > 7 THEN YEAR(CURDATE()) ELSE YEAR(CURDATE()) - 1 END - ( YEAR ) ), ' ', NAME) AS TEAM,
				ID
			FROM
				TBL_TEAM 
			) T ON 
			S.TEAMID = T.ID
		WHERE
			TXT NOT IN ( 'No Comment')
		ORDER BY 
			SURVEY_DATE DESC
		Limit 15";

	$tbl= mysqli_query($con,$sql);
	    

while($row = mysqli_fetch_array($tbl)) {

	echo "<div class='testimonial col-md-12' >
		<div class='author-photo'>
			<h4 style='color:white'> Game Date " . $row['GAME_DATE'] . " </h4>
		</div>
		<div class='testimonial-bubble'>
			<blockquote>
				<!-- Quote -->
				<p class='quote'>
					Survey Date: " . $row['SURVEY_DATE'] . " </br> Comments: " . $row['TXT'] . "
				</p>
				<!-- Team Info -->
				<cite class='author-info'>
					" . $row['TEAM'] . "
				</cite>
			</blockquote>
			<div class='sprite arrow-speech-bubble'></div>
		</div>
	     </div>";	
}

mysqli_close($con);
?>