<!DOCTYPE html>
<?php

$mysqli = new mysqli('localhost','mante3_rbeall','3VTMx9@!8EZs','mante3_team_info');

$stmt = $mysqli->prepare("SELECT HEADLINE,  IMG, NEWSTEXT, NEWSDATE FROM TBL_NEWS WHERE ID = ?");

$id = strval($_GET['id']);

/* bind parameters for markers */
mysqli_stmt_bind_param($stmt, "s", $id);
    
/* execute query */
mysqli_stmt_execute($stmt);

/* bind result variables */
mysqli_stmt_bind_result($stmt, $headline, $img, $newstext, $newsdate);

/* fetch value */
mysqli_stmt_fetch($stmt);


$txt = str_replace("\\n", "<br/><br/>",strval($newstext));

echo "<h3 id='storyHeadline'>". $headline . "</h3>                 
  <div class='single-post-info'>  <i class='glyphicon glyphicon-time'></i>" . $newsdate . "   </div>
  <div class='single-post-image'> <img  id='imgsrc' src='/web/images/news/". $img. "'>  </div>
  <div class='single-post-content'>   <p>" . $txt . "</p>         </div>";   
 
/* close statement and connection */
$stmt->close();

/* close connection */
$mysqli->close();                                
?>