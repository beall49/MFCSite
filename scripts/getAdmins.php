<?php


$con = mysqli_connect('localhost','mante3_rbeall','3VTMx9@!8EZs','mante3_team_info');
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}

$bit = "0";
$id = strval($_GET['q']);

if ($id==1){
   $bit = "1 OR T.PERSON_ID IS NULL";
}
	
/*$sql="SELECT NAME, EMAIL, PHONE, TITLE, IMAGE FROM TBL_PERSON WHERE TITLE IS NOT NULL ORDER BY PERSON_ID";*/
$sql="	SELECT * FROM (
		SELECT 
			IFNULL(P.NAME, 'OPEN') AS NAME,
			IFNULL(P.EMAIL, 'OPEN') AS EMAIL, 
			IFNULL(P.PHONE, 'OPEN') AS PHONE,
			T.DESCR AS TITLE, 
			IFNULL(P.IMAGE , '/images/placeholder_male.png') AS IMAGE,
			CASE WHEN T.PERSON_ID IS NULL THEN 99  ELSE T.TITLE_ID END AS ID
		FROM 
			TBL_TITLE T
		LEFT JOIN 
			TBL_PERSON P ON
			T.PERSON_ID = P.PERSON_ID
		WHERE
			P.BOD = " . $bit . " 	
		) A
	ORDER BY ID";

$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result)) {
	echo "
	<!--start person -->   	
	<div class='col-md-4'>                      
		<div class='team-member'>
			<!-- Team Member Photo  -->   
			<div class='team-member-image'>
				<img src=". $row['IMAGE'] . ">
			</div>
			<div class='team-member-info'>
				<ul  class='list-group' style='margin-bottom:1px'>
					<!-- Team Member Info & Social Links -->     
					<li class='list-group-item'>". $row['NAME'] . "</li>                       
					<li class='list-group-item'>". $row['TITLE'] . "</li>
					<li class='list-group-item'> 
						<i class='glyphicon glyphicon-phone-alt '> </i> <a href='tel:'". $row['PHONE'] . "'>". $row['PHONE'] . "</a>  
					</li>
					<li class='list-group-item'> <i class='glyphicon glyphicon-envelope'> </i> <a href='mailto:'". $row['EMAIL'] . "'>". $row['EMAIL'] . "</a></li>
				</ul>
			</div>
		</div>
	</div>  			
	<!--end person -->";
}

mysqli_close($con);
?>