$(document).ready(
	function() {			
		getData ( 'static/navbar.html' , '#menu');
		getData ( 'static/headerLogo.html' , '#headerLogo');
		getData ( 'static/footer.html' , '#footer');
		
		var myPage = location.search.split( 'Page=' )[1] ;
		getData( "scripts/getNews.php?q=" + myPage , "#news");            	            
	}
);


function nextPage(){
	var myPage = ( location.search.split('Page=')[1] );
	if (typeof myPage == "undefined") {
		myPage = 1;
	}	
	if(myPage<8){
		window.location = "/clubnews.html?Page=" + (+myPage + 1).toString();
	}	
}

function prevPage(){
	var myPage = ( location.search.split('Page=')[1] );
	if(myPage>1){
		window.location = "/clubnews.html?Page=" + (+myPage-1).toString();
	}	
}

function getData(file, element){	
	$.get(file, 
		function( data ) {
			$( element ).append( data );			 
		}
	);
}