$(document).ready(
	function() {
		$('#menu').load('../static/navbar.html', function() {});
		$('#headerLogo').load('../static/headerLogo.html', function() {});
		$('#fields').load('../static/fields.html', function() {});        
		$('#goals').load('../static/goals.html', function() {});   
		$('#accounting').load('../static/accounting.html', function() {});   
		$('#fundraiser').load('../static/fundraiser.html', function() {});
		$('#registration').load('../static/registration.html', function() {});       
		$('#courses').load('../static/courses.html', function() {});            

		$('#board').load('../static/board.html', function() {}); 
		$('#documents').load('../static/documents.html', function() {});     
		$('#footer').load('../static/footer.html', function() {}); 
                                      
	}
);


$(".dropdown-menu li a").click(
	function(){
		var selText = $(this).text();
		var component = $(this).parents('.btn-group').find('.dropdown-toggle');
		component.html(selText +' <span class="caret"></span>');
		var id = component.attr("id");
		if (id == "btnRef"){
			$.get( "../scripts/getRefPricing.php?q="+selText , 
					function( data ) {
						$( "#ageGroup" ).empty( ).append( data );					
					}
				);			
		}
	}
);

$('.getSrc').click(
	function() {
		var src =$(this).attr('src');
		$('.showPic').attr('src', src);
	}
);

	