$(document).ready(function() {
    $('#menu').load('../static/navbar.html', function() {});
    $('#headerLogo').load('../static/headerLogo.html', function() {});
    $('#footer').load('../static/footer.html', function() {});              
               
});

$(".dropdown-menu li a").click(function(){
	var selText = $(this).text();
	
	$(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');

	$.get( "../scripts/getEmailLists.php?q=" + selText, function( data ) {
		$( "#emailList").html(data );
	});
}); 