		$(document).ready(function() {
			$('#menu').load('../static/navbar.html', function() {});
			$('#footer').load('../static/footer.html', function() {});	         	            
			createCharts();	
			$.get( '../scripts/getResultsTbl.php?', function( data ) {
				console.log("success");
				$( "#comments").html( data );			 
			});		 
		});

	
		function createCharts(){
			$.getJSON('../scripts/getResults.php?', 
				function(data){
					$.each(data, function(index){							
							var pieData = [
								{
									value: parseInt(data[index].YES),
									color:"#5CB85C",
									label:((index < 4 ) ? "YES" : "Excellent")
								},
								{
									value : parseInt(data[index].NO),
									color : ((index < 4 ) ? "#D9534F" : "#337AB7"),
									label :	((index < 4 ) ? "No" : "Good")
								},
								{
									value : parseInt(data[index].IDK),
									color : "#F0AD4E",
									label :((index < 4 ) ? "Unknown" : "Fair")
								},
								{
									value : parseInt(data[index].POOR),
									color : "#D9534F",
									label : ((index < 4 ) ? "N/A" : "Poor")
								}
							]
							var pieOptions = {animateScale : true	};
							var cht = $(data[index].Question).get(0).getContext("2d");			
							new Chart(cht).Pie(pieData, pieOptions);
					});
				});
		}