		
function disableComponent(component, bool){
	component.prop('disabled', bool);
}

function changeClass(component, changeToo){
	var changeFrom = (( changeToo == 'btn-danger') ? 'btn-info' : 'btn-danger' );
	component.removeClass(changeFrom ).addClass(changeToo);
}
	
function componentHide(component, bool){
	component.prop('hidden', bool);
}	



$(document).ready(
	function() {
		$('#menu').load('../static/navbar.html', function() {});
		$('#headerLogo').load('../static/headerLogo.html', function() {});
		$('#footer').load('../static/footer.html', function() {});	         	            
		$.get( "../scripts/getTeamList.php", function( data ) {
			$( "#teamlist" ).append( data );			 
		});
		msieversion();
	});



$("#gameDate").change(
	function(){
		changeClass($(this), 'btn-info');
		disableComponent($(".dropdown-menu li a").parents('.btn-group').find('.dropdown-toggle'), false);
	});

$("#gameDate").blur(
	function(){
		changeClass($(this), 'btn-info');
		disableComponent($(".dropdown-menu li a").parents('.btn-group').find('.dropdown-toggle'), false);
	});

$(".dropdown-menu").on('click', 'li a', 
	function(){		
		var dropdown = $(".dropdown-menu li a").parents('.btn-group').find('.dropdown-toggle');
		var btnSubmit = $("#btnSubmit");
		dropdown.html($(this).text() +' <span class="caret"></span>' );
		
		changeClass(dropdown, 'btn-info');			
		changeClass(btnSubmit, 'btn-info');
		disableComponent(btnSubmit, false);
		
		$("#teamID").text($(this).parents('li').attr("data-team"));				        		
	});	


		
$('#btnSubmit').on('click', 
	function(){		
		var items = {};			
		var txt = $('textarea#txtQ6').val()
		if(!$.trim(txt).length) { 
			txt = "No Comment";
		}
		items["txt"] 	  = txt;			
		items["team"] 	  = $("button#teamID").text();
		items["gamedate"] = $("#gameDate").val();
		for (var x=1 ; x < 6; x++){				
			var radioButton = $("input[name=optionsRadios" + x.toString() + "]:checked", "#survey").val(); 
			var id =  "q" + x.toString();
			items[id] = radioButton;
		}
		var jsonArr = {"results":items};
		
		$.ajax({  
			type: 'POST',  
			url: '../scripts/setSurvey.php',  
			data: JSON.stringify(jsonArr),  				
		});
		
		$('#survey')[0].reset();		
		componentHide($("#successAlert"), false);
		var dropdown = $(".dropdown-menu li a").parents('.btn-group').find('.dropdown-toggle');
		var btnSubmit = $("#btnSubmit");
	
		changeClass(dropdown, 'btn-danger');			
		changeClass(btnSubmit, 'btn-danger');
		changeClass($("#gameDate"), 'btn-danger');
		disableComponent(btnSubmit, true);
		disableComponent(dropdown, true);
	});
	
function msieversion() {
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");
	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))     {
		componentHide($("#ieDanger"), false);
	}
}	