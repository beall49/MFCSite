$.getJSON('scripts/getTeamsJ.php',  function(data) {
		  var listItems=[];
		  $.each( data, function( index) {
			var txt = ""+
			"<!--Team Listing --> " +
			"<div class='col-sm-4'> " +
				"<div class='shop-item'>" +
					"<div class='image'> " +
					"<a href='"+ data[index].TEAMSITE + "' target='_blank'> <img style='height:206px; width:344px' src='" + data[index].IMAGE + "'> " +
					"</a> " +
			 		"</div>" +
			 		"<div class='title'>" +
			 			"<h3>" + data[index].IDEN  +"</h3> " +
					"</div> " +
					"<div >" +
					  "<ul class='list-group list-group-flush text-center'> " +
						 "<li class='list-group-item'><i class='icon-ok text-danger'></i><a href=mailto:" + data[index].COACHEMAIL  + "> Coach: " + data[index].COACH + "</a></li> " +
						 "<li class='list-group-item'><i class='icon-ok text-danger'></i><a href=mailto:" + data[index].ACOACHEMAIL + "> Assistant Coach: " + data[index].ACOACH + " </a></li> " +
						 "<li class='list-group-item'><i class='icon-ok text-danger'></i><a href=mailto:" + data[index].MGREMAIL + "> Manager: " + data[index].MGR + " </a></li> " +
					  "</ul> " +
					"</div> " +
				"</div> " +
			"</div> " +		
			"<!--End Team Listing --> " 			 
			listItems.push(txt);			 
		  });
		  $('#teamInfo').html(listItems.join(""));
	    })            	            