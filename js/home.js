$(document).ready(
	function() {
		$('#menu').load('../static/navbarNotFixed.html', function() {});
		$('#footer').load('../static/footer.html', function() {});	 
		getData( "../scripts/getNewsSlider.php", "#slides" );
		getData( "../scripts/getEvents.php", "#events" );
		getData( "../scripts/getNewsIndex.php", "#smallNews" );
		getData( "../scripts/getBoardDate.php", "#boardDate" );
		$('#banner').fadeIn(3000, function() { });
			
		
	});
	

      
function getCalendar(){
	$('#calendar').load('../static/modal/calendarModal.html', function() {
		$('#calendarModal').modal('show'); 
	});
			
}

function getKeeper(){
	$('#keeper').load('../static/modal/keeperModal.html', function() {
		$('#keeperModal').modal('show'); 
	});
			
}

	
function closeModal(element){
	$(element).modal('hide'); 		
}	


function getData(file, element){
	$.get( file, function( data ) {
		if ( element =="#events"){
			$( "#eventPhantom" ).append( data );
		}
		$( element ).append( data );
			 
	});
}
	