function getData(file, element){	
	$.get(file, 
		function( data ) {
			$( element ).append( data );
			if ( element == '#news'){
				var headline =   $("#storyHeadline").html();           				         				         	
				document.title =  headline;				         								 
				$( "meta[property='og\\:title']" ).attr("content", headline);

			}
		}
	);
}  

$(document).ready(
	function() {
		var id = location.search.split('id=')[1] ? location.search.split('id=')[1] : '1';
		$( "meta[property='og\\:url']" ).attr("content", location);
		$('#menu').load('static/navbar.html', function() {});
		$('#headerLogo').load('static/headerLogo.html', function() {});
		$('#footer').load('static/footer.html', function() {});	
		getData('scripts/getTop5News.php', '#recentNews');
		getData( ' scripts/getStoryById.php?id=' + id, '#news');
	}
);

       


